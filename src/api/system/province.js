import request from '@/utils/request'

// 查询省份列表
export function listProvince(query) {
  return request({
    url: '/system/province/list',
    method: 'get',
    params: query
  })
}

// 查询省份详细
export function getProvince(code) {
  return request({
    url: '/system/province/' + code,
    method: 'get'
  })
}

// 新增省份
export function addProvince(data) {
  return request({
    url: '/system/province',
    method: 'post',
    data: data
  })
}

// 修改省份
export function updateProvince(data) {
  return request({
    url: '/system/province',
    method: 'put',
    data: data
  })
}

// 删除省份
export function delProvince(code) {
  return request({
    url: '/system/province/' + code,
    method: 'delete'
  })
}
