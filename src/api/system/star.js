import request from '@/utils/request'

// 查询用户sku收藏列表
export function listStar(query) {
  return request({
    url: '/system/star/list',
    method: 'get',
    params: query
  })
}

// 查询用户sku收藏详细
export function getStar(userId) {
  return request({
    url: '/system/star/' + userId,
    method: 'get'
  })
}

// 新增用户sku收藏
export function addStar(data) {
  return request({
    url: '/system/star',
    method: 'post',
    data: data
  })
}

// 修改用户sku收藏
export function updateStar(data) {
  return request({
    url: '/system/star',
    method: 'put',
    data: data
  })
}

// 删除用户sku收藏
export function delStar(userId) {
  return request({
    url: '/system/star/' + userId,
    method: 'delete'
  })
}
