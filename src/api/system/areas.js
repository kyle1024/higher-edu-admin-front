import request from '@/utils/request'

// 查询【请填写功能名称】列表
export function listAreas(query) {
  return request({
    url: '/system/areas/list',
    method: 'get',
    params: query
  })
}

// 查询【请填写功能名称】详细
export function getAreas(code) {
  return request({
    url: '/system/areas/' + code,
    method: 'get'
  })
}

// 新增【请填写功能名称】
export function addAreas(data) {
  return request({
    url: '/system/areas',
    method: 'post',
    data: data
  })
}

// 修改【请填写功能名称】
export function updateAreas(data) {
  return request({
    url: '/system/areas',
    method: 'put',
    data: data
  })
}

// 删除【请填写功能名称】
export function delAreas(code) {
  return request({
    url: '/system/areas/' + code,
    method: 'delete'
  })
}
