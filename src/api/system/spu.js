import request from '@/utils/request'

// 查询商品spu列表
export function listSpu(query) {
  return request({
    url: '/system/spu/list',
    method: 'get',
    params: query
  })
}

// 查询商品spu详细
export function getSpu(id) {
  return request({
    url: '/system/spu/' + id,
    method: 'get'
  })
}

// 新增商品spu
export function addSpu(data) {
  return request({
    url: '/system/spu',
    method: 'post',
    data: data
  })
}

// 修改商品spu
export function updateSpu(data) {
  return request({
    url: '/system/spu',
    method: 'put',
    data: data
  })
}

// 删除商品spu
export function delSpu(id) {
  return request({
    url: '/system/spu/' + id,
    method: 'delete'
  })
}
