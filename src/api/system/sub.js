import request from '@/utils/request'

// 查询用户订单子列表
export function listSub(query) {
  return request({
    url: '/system/sub/list',
    method: 'get',
    params: query
  })
}

// 查询用户订单子详细
export function getSub(id) {
  return request({
    url: '/system/sub/' + id,
    method: 'get'
  })
}

// 单个转赠
export function addSub(data) {
  return request({
    url: '/system/order/sub',
    method: 'post',
    data: data
  })
}

// 批量转赠
export function addSubBatch(data) {
  return request({
    url: '/system/order/sub/batch',
    method: 'post',
    data: data
  })
}

// 修改用户订单子
export function updateSub(data) {
  return request({
    url: '/system/sub',
    method: 'put',
    data: data
  })
}

// 删除用户订单子
export function delSub(id) {
  return request({
    url: '/system/sub/' + id,
    method: 'delete'
  })
}
